# Mage2 Module YerAb Prequest

    yerab/module-prequest

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)


## Main Functionalities
Sending price requests via popup instead of showing price on product page

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/YerAb`
 - Rename unzipped folder to `Prequest`
 - Enable the module by running `php bin/magento module:enable YerAb_Prequest`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require yerab/module-prequest`
 - enable the module by running `php bin/magento module:enable YerAb_Prequest`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Enable Extension (prequest/general/enabled)


## Specifications

 - Model
	- PriceRequest

