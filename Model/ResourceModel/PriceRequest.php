<?php


namespace YerAb\Prequest\Model\ResourceModel;

class PriceRequest extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('yerab_prequest_pricerequest', 'pricerequest_id');
    }
}
