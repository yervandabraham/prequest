<?php


namespace YerAb\Prequest\Model\ResourceModel\PriceRequest;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \YerAb\Prequest\Model\PriceRequest::class,
            \YerAb\Prequest\Model\ResourceModel\PriceRequest::class
        );
    }
}
