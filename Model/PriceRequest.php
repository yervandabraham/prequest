<?php


namespace YerAb\Prequest\Model;

use YerAb\Prequest\Api\Data\PriceRequestInterfaceFactory;
use YerAb\Prequest\Api\Data\PriceRequestInterface;
use Magento\Framework\Api\DataObjectHelper;

class PriceRequest extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $pricerequestDataFactory;

    protected $_eventPrefix = 'yerab_prequest_pricerequest';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceRequestInterfaceFactory $pricerequestDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \YerAb\Prequest\Model\ResourceModel\PriceRequest $resource
     * @param \YerAb\Prequest\Model\ResourceModel\PriceRequest\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PriceRequestInterfaceFactory $pricerequestDataFactory,
        DataObjectHelper $dataObjectHelper,
        \YerAb\Prequest\Model\ResourceModel\PriceRequest $resource,
        \YerAb\Prequest\Model\ResourceModel\PriceRequest\Collection $resourceCollection,
        array $data = []
    ) {
        $this->pricerequestDataFactory = $pricerequestDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve pricerequest model with pricerequest data
     * @return PriceRequestInterface
     */
    public function getDataModel()
    {
        $pricerequestData = $this->getData();
        
        $pricerequestDataObject = $this->pricerequestDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $pricerequestDataObject,
            $pricerequestData,
            PriceRequestInterface::class
        );
        
        return $pricerequestDataObject;
    }
}
