<?php


namespace YerAb\Prequest\Model\Data;

use YerAb\Prequest\Api\Data\PriceRequestInterface;

class PriceRequest extends \Magento\Framework\Api\AbstractExtensibleObject implements PriceRequestInterface
{

    /**
     * Get pricerequest_id
     * @return string|null
     */
    public function getPricerequestId()
    {
        return $this->_get(self::PRICEREQUEST_ID);
    }

    /**
     * Set pricerequest_id
     * @param string $pricerequestId
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setPricerequestId($pricerequestId)
    {
        return $this->setData(self::PRICEREQUEST_ID, $pricerequestId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \YerAb\Prequest\Api\Data\PriceRequestExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \YerAb\Prequest\Api\Data\PriceRequestExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \YerAb\Prequest\Api\Data\PriceRequestExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get email
     * @return string|null
     */
    public function getEmail()
    {
        return $this->_get(self::EMAIL);
    }

    /**
     * Set email
     * @param string $email
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Get comment
     * @return string|null
     */
    public function getComment()
    {
        return $this->_get(self::COMMENT);
    }

    /**
     * Set comment
     * @param string $comment
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
}
