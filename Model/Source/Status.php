<?php
namespace YerAb\Prequest\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * @api
 * @since 100.1.0
 */
class Status implements OptionSourceInterface
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @since 100.1.0
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'New',
                'label' => __('New')
            ],
            [
                'value' => 'In Progress',
                'label' => __('In Progress')
            ],
            [
                'value' => 'Closed',
                'label' => __('Closed')
            ]
        ];
    }
}
