<?php


namespace YerAb\Prequest\Model;

use YerAb\Prequest\Model\ResourceModel\PriceRequest\CollectionFactory as PriceRequestCollectionFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use YerAb\Prequest\Api\PriceRequestRepositoryInterface;
use YerAb\Prequest\Api\Data\PriceRequestSearchResultsInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use YerAb\Prequest\Api\Data\PriceRequestInterfaceFactory;
use YerAb\Prequest\Model\ResourceModel\PriceRequest as ResourcePriceRequest;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\NoSuchEntityException;

class PriceRequestRepository implements PriceRequestRepositoryInterface
{

    protected $priceRequestCollectionFactory;

    protected $dataObjectHelper;

    private $storeManager;

    protected $dataPriceRequestFactory;

    protected $priceRequestFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;


    /**
     * @param ResourcePriceRequest $resource
     * @param PriceRequestFactory $priceRequestFactory
     * @param PriceRequestInterfaceFactory $dataPriceRequestFactory
     * @param PriceRequestCollectionFactory $priceRequestCollectionFactory
     * @param PriceRequestSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePriceRequest $resource,
        PriceRequestFactory $priceRequestFactory,
        PriceRequestInterfaceFactory $dataPriceRequestFactory,
        PriceRequestCollectionFactory $priceRequestCollectionFactory,
        PriceRequestSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->priceRequestFactory = $priceRequestFactory;
        $this->priceRequestCollectionFactory = $priceRequestCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPriceRequestFactory = $dataPriceRequestFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \YerAb\Prequest\Api\Data\PriceRequestInterface $priceRequest
    ) {
        /* if (empty($priceRequest->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $priceRequest->setStoreId($storeId);
        } */
        
        $priceRequestData = $this->extensibleDataObjectConverter->toNestedArray(
            $priceRequest,
            [],
            \YerAb\Prequest\Api\Data\PriceRequestInterface::class
        );
        
        $priceRequestModel = $this->priceRequestFactory->create()->setData($priceRequestData);
        
        try {
            $this->resource->save($priceRequestModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the priceRequest: %1',
                $exception->getMessage()
            ));
        }
        return $priceRequestModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($priceRequestId)
    {
        $priceRequest = $this->priceRequestFactory->create();
        $this->resource->load($priceRequest, $priceRequestId);
        if (!$priceRequest->getId()) {
            throw new NoSuchEntityException(__('PriceRequest with id "%1" does not exist.', $priceRequestId));
        }
        return $priceRequest->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->priceRequestCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \YerAb\Prequest\Api\Data\PriceRequestInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \YerAb\Prequest\Api\Data\PriceRequestInterface $priceRequest
    ) {
        try {
            $priceRequestModel = $this->priceRequestFactory->create();
            $this->resource->load($priceRequestModel, $priceRequest->getPricerequestId());
            $this->resource->delete($priceRequestModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PriceRequest: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($priceRequestId)
    {
        return $this->delete($this->get($priceRequestId));
    }
}
