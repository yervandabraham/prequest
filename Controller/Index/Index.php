<?php
namespace YerAb\Prequest\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \YerAb\Prequest\Helper\Data
     */
    public $helper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \YerAb\Prequest\Helper\Data $helper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \YerAb\Prequest\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->helper = $helper;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute index action
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $m = 'No price request is sent.';
        $t = "error";
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $model = $this->_objectManager->create(\YerAb\Prequest\Model\PriceRequest::class);
            $data['status'] = 'New';
            $model->setData($data);
            try {
                $model->save();
                $m = __('You have sent the price request successfully.');
                $t = 'success';
            } catch (LocalizedException $e) {
                $m = $e->getMessage();
            } catch (\Exception $e) {
                $m = __('Something went wrong while sending the price request.');
            }
        }
        return  $result->setData(array("message" => $m, "type" => $t));
    }
}
