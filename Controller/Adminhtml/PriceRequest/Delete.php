<?php


namespace YerAb\Prequest\Controller\Adminhtml\PriceRequest;

class Delete extends \YerAb\Prequest\Controller\Adminhtml\PriceRequest
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('pricerequest_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\YerAb\Prequest\Model\PriceRequest::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the price request.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['pricerequest_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a price request to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
