<?php


namespace YerAb\Prequest\Api\Data;

interface PriceRequestSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PriceRequest list.
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \YerAb\Prequest\Api\Data\PriceRequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
