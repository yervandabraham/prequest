<?php


namespace YerAb\Prequest\Api\Data;

interface PriceRequestInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const EMAIL = 'email';
    const NAME = 'name';
    const CREATED_AT = 'created_at';
    const COMMENT = 'comment';
    const SKU = 'sku';
    const PRICEREQUEST_ID = 'pricerequest_id';
    const STATUS = 'status';

    /**
     * Get pricerequest_id
     * @return string|null
     */
    public function getPricerequestId();

    /**
     * Set pricerequest_id
     * @param string $pricerequestId
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setPricerequestId($pricerequestId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \YerAb\Prequest\Api\Data\PriceRequestExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \YerAb\Prequest\Api\Data\PriceRequestExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \YerAb\Prequest\Api\Data\PriceRequestExtensionInterface $extensionAttributes
    );

    /**
     * Get email
     * @return string|null
     */
    public function getEmail();

    /**
     * Set email
     * @param string $email
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setEmail($email);

    /**
     * Get comment
     * @return string|null
     */
    public function getComment();

    /**
     * Set comment
     * @param string $comment
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setComment($comment);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setSku($sku);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     */
    public function setStatus($status);
}
