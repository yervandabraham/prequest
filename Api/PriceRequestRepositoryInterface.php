<?php


namespace YerAb\Prequest\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PriceRequestRepositoryInterface
{

    /**
     * Save PriceRequest
     * @param \YerAb\Prequest\Api\Data\PriceRequestInterface $priceRequest
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \YerAb\Prequest\Api\Data\PriceRequestInterface $priceRequest
    );

    /**
     * Retrieve PriceRequest
     * @param string $pricerequestId
     * @return \YerAb\Prequest\Api\Data\PriceRequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($pricerequestId);

    /**
     * Retrieve PriceRequest matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \YerAb\Prequest\Api\Data\PriceRequestSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PriceRequest
     * @param \YerAb\Prequest\Api\Data\PriceRequestInterface $priceRequest
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \YerAb\Prequest\Api\Data\PriceRequestInterface $priceRequest
    );

    /**
     * Delete PriceRequest by ID
     * @param string $pricerequestId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($pricerequestId);
}
