<?php
namespace YerAb\Prequest\Block;

class Requestprice extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \YerAb\Prequest\Helper\Data
     */
    public $helper;

    /**
     * Requestprice constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \YerAb\Prequest\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \YerAb\Prequest\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Get product SKU.
     *
     * @return mixed
     */
    public function getProductSku()
    {
        return $this->helper->getProductSku();
    }

    /**
     * Get product SKU.
     *
    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRequestpriceUrl()
    {
        return $this->helper->getBaseUrl().'prequest/index/index/';
    }

    /**
     * Check if extension enabled
     *
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->helper->isEnabled();
    }
}
